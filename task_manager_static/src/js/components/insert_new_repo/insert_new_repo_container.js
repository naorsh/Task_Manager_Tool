/**
 * Created by naor on 25/08/16.
 */
import React, { Component, PropTypes } from 'react';
// import * as stores from '../../stores';
import InsertNewRepoComponent from './insert_new_repo_component';
import NewRepoSubmitted from './new_repo_submitted';
import fetch from 'isomorphic-fetch'


class InsertNewRepo extends Component {
    constructor(props) {
        super(props);
        this.submit_new_repo = this.submit_new_repo.bind(this)
    }

     submit_new_repo() {
         let link_to_git_input = document.getElementById('link_to_git_input').value;
         let content_main = document.getElementById('content_main').value;
        let form_data ={
            repo_url: link_to_git_input,
            main_file_path:  content_main
        };
        this.props.TasksStore.loading = true;
        fetch("/api/task_manager_app/insert_new_repo/",
            {
                method: "POST",
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(form_data)
            },
         ).then( response => {
             this.props.TasksStore.loading = false;
             // this.setState({didReturnResult: true});
             this.context.router.push('/api/task_manager_app/new_repo_submitted/');
            let outputDiv =document.getElementById('output_div');
             if (response["status"] === 200) {
                  outputDiv.innerHTML += "<h2>הפרוייקט נוסף בהצלחה</h2><br>";
             }
             else {
                 response.json().then(jsonData => {
                     let lines = jsonData.split('\n');
                     outputDiv.innerHTML += "<h2>התרחשה תקלה בזמן הוספת הפרוייקט </h2><br>";
                     lines.forEach(line => {
                         outputDiv.innerHTML += "<p align='left'>" + line + "</p>" ;
                     });
                 })
             }
        });
    };
    render() {
        return (
            <InsertNewRepoComponent submit_new_repo={this.submit_new_repo}/>
        );
    }
}
InsertNewRepo.contextTypes = {
    router: React.PropTypes.object
};
export default InsertNewRepo;