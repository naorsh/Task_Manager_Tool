/**
 * Created by naor on 25/08/16.
 */
import React from 'react';

const IntervalRowComponent = props => {
    return(
        <tr>
            <td>{props.interval.period}</td>
            <td>{props.interval.every}</td>
            <td>
                <button className="btn btn-danger" onClick={props.stopIntervalTask}>
                    <image className="glyphicon glyphicon-stop"/>
                </button>
            </td>
            <td>
                <button className="btn btn-danger" onClick={props.deleteIntervalTask}>
                    <image className="glyphicon glyphicon-trash"/>
                </button>
            </td>
            <td>
                <button className="btn btn-danger" onClick={props.startIntervalTask}>
                    <image className="glyphicon glyphicon-play"/>
                </button>
            </td>

        </tr>
    );
};

export default IntervalRowComponent;