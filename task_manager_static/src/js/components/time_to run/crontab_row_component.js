/**
 * Created by lidor08 on 12/09/16.
 */
import React from 'react';

const CrontabRowComponent = props => {
    return(
        <tr>
            <td>{props.crontab.minute}</td>
            <td>{props.crontab.hour}</td>
            <td>{props.crontab.day_of_week}</td>
            <td>{props.crontab.day_of_month}</td>
            <td>{props.crontab.month_of_year}</td>
            <td>
                <button className="btn btn-danger" onClick={props.stopCrontabTask}>
                    <image className="glyphicon glyphicon-stop"/>
                </button>
            </td>
            <td>
                <button className="btn btn-danger" onClick={props.deleteCrontabTask}>
                    <image className="glyphicon glyphicon-trash"/>
                </button>
            </td>
            <td>
                <button className="btn btn-danger" onClick={props.startCrontabTask}>
                    <image className="glyphicon glyphicon-play"/>
                </button>
            </td>
        </tr>
    );
};

export default CrontabRowComponent;