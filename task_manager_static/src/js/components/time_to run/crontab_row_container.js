/**
 * Created by lidor08 on 12/09/16.
 */
import React from 'react';
import CrontabRowComponent from './crontab_row_component'
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'

class CrontabRow extends React.Component {
        constructor(props) {
        super(props);
    };

    render() {
        return (
            <CrontabRowComponent crontab={this.props.crontab} startCrontabTask={this.startCrontabTask.bind(this)}
                                 deleteCrontabTask={this.deleteCrontabTask.bind(this)}  stopCrontabTask={this.stopCrontabTask.bind(this)}/>

        );

    }
    stopCrontabTask(){
            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };

            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.stopScheduleTask(formData, function(response) {
                console.log(response);
            })
    };

    deleteCrontabTask(){

            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };

            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.deleteScheduleTask(formData,(newData) => {
            console.log('task lis after delete', newData.data);
            this.props.ProjectTasks[0] = newData;
            })

    };

    startCrontabTask(){

            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };


            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.startScheduleTask(formData, function(response) {
                console.log(response);
            })
    };


}

export default CrontabRow