/**
 * Created by lidor08 on 13/10/16.
 */
import React from 'react'
import {observer} from 'mobx-react';
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'
import AddNewContrabTask from '../insert_new_time_to_run/add_new_corntab_task'

@observer
class PeriodicTasksTable extends React.Component {
    constructor(props) {
        console.log("test")
        super(props);
        this._onButtonClick = this._onButtonClick.bind(this);
        this._offButtonClick = this._offButtonClick.bind(this);
        this.runProjectNow =this.runProjectNow.bind(this);
        this.updateRepo =this.updateRepo.bind(this);
        this.deleteRepo =this.deleteRepo.bind(this);
        this.props.deleteButton;
        this.state = { showAddTaskDiv: false,
                        showComponent: false,
                        deleteButton:false,
                        addExitButton:true,
        };

    }
        _onButtonClick() {
            this.setState({
              showComponent: true,
                addExitButton:false,
            });
       }
       _offButtonClick() {
            this.setState({
              showComponent: false,
                addExitButton:true,
            });
       }

       runProjectNow() {
        let formData = {
            main_file_path: this.props.ChosenGitRepoTask.main_file_path,
            repo_virtual_env: this.props.ChosenGitRepoTask.repo_virtual_env,
        };

        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        console.log("this is the file path, before get request", this.props.ChosenGitRepoTask.repo_virtual_env);
        api.runTaskNow(formData, (response) => {
            console.log(response);
        });
        console.log("project is running")
    };



    updateRepo() {
        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.updateRepoByName(this.props.ChosenGitRepoTask.repo_name, (response) => {
            console.log(response);
        });
        console.log("project was updated")
    }

    deleteRepo() {
        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.deleteRepoByName(this.props.ChosenGitRepoTask.repo_name, (response) => {
            this.props.ProjectTasks.splice(this.props.ProjectTasks.indexOf(this.props.periodic_task), 1);

        });
        this.forceUpdate();
        console.log("project was deleted");
    }



    render(){
        console.log("render");

        return(
            <div className="table_div">
                <div className="project_name"><h3 id="pro_name">{this.props.ChosenGitRepoTask.repo_name}</h3></div>
                <table className="table">
                        <thead>
                        <tr>
                            <th className="text-center">שם משימה</th>
                            <th className="text-center">זמן רצה</th>
                            <th className="text-center">מתי רץ בפעם האחרונה</th>
                            <th className="text-center">פעולות</th>
                            <th>
                                <div className="dropdown">
                                    <a href="#" className="lidor btn btn-simple dropdown-toggle" data-toggle="dropdown">
                                        <i className="material-icons" id="options-icon">build</i>
                                        אפשרויות
                                        <b className="caret"></b>
                                    </a>
                                    <ul className="dropdown-menu">
                                        <li><a href="#" onClick={this.runProjectNow}>הרץ פרויקט כעת</a></li>
                                        <li><a href="#" onClick={this.deleteRepo}> מחק פרויקט</a></li>
                                        <li><a href="#" onClick={this.updateRepo}>עדכן פרויקט</a></li>
                                    </ul>
                                </div>
                            </th>
                        </tr>
                        </thead>
                        <tbody className="info">{this.props.rows}</tbody>
                  </table>

                {this.state.showComponent ?
                    <div>
                       <AddNewContrabTask ChosenGitRepoTask={this.props.ChosenGitRepoTask} ProjectTasks={this.props.ProjectTasks}/>
                    </div>:
                   null
                }
                {this.state.addExitButton ?
                    <div id="add-task-button">
                        <button className="btn btn-primary btn-fab btn-fab-mini btn-round"  onClick={this._onButtonClick}>
                            <i className="material-icons">add</i>
                        </button>
                    </div>:
                    <div id="add-task-button">
                        <button className="btn btn-primary btn-fab btn-fab-mini btn-round"   onClick={this._offButtonClick}>
                            <i className="material-icons">clear</i>
                        </button>
                    </div>
                }

            </div>
            );
    }
}


export default PeriodicTasksTable;
