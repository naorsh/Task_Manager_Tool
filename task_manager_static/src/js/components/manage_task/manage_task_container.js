/**
 * Created by naor on 25/08/16.
 */
import React from 'react'
import ManageTaskComponent from './manage_task_component'
import { observer } from 'mobx-react';
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'

@observer
class ManageTask extends React.Component {

    constructor(props) {
        super(props);
        this.runProjectNow = this.runProjectNow.bind(this);
        this.updateRepo = this.updateRepo.bind(this);
    };

    runProjectNow() {
        let formData = {
            main_file_path: this.props.ChosenGitRepoTask.main_file_path,
            repo_virtual_env: this.props.ChosenGitRepoTask.repo_virtual_env
        };

        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        console.log("this is the file path, before get request", this.props.ChosenGitRepoTask.repo_virtual_env);
        api.runTaskNow(formData, (response) => {
            console.log(response);
        });
        console.log("project is running")
    };

    updateRepo() {
        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.updateRepoByName(this.props.ChosenGitRepoTask.repo_name, (response) => {
            console.log(response);
        });
        console.log("project was updated")
    }

    render() {
        return (
            <ManageTaskComponent chosenTask={this.props.ChosenGitRepoTask} periodicTasks={this.props.ProjectTasks}
                                 runProjectNow={this.runProjectNow} updateRepo={this.updateRepo}/>
        );
    };



}
export default ManageTask;