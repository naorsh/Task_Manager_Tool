/**
 * Created by naor on 25/08/16.
 */
// from task_manager.modals import ProjectTasks
import React from 'react'
import IntervalRow from '../time_to run/interval_row_container'
import InsertNewIntervalComponent from '../insert_new_time_to_run/insert_new_interval_container'
import InsertNewCrontabComponent from '../insert_new_time_to_run/add_new_corntab_task'
import CrontabRow from '../time_to run/crontab_row_container'
import {observer} from 'mobx-react'


@observer
class ManageTaskComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    createTableOfInterval() {
        let table_rows = [];
        console.log('chosen task',this.props.ChosenGitRepoTask.id);
        let id =this.props.ChosenGitRepoTask.id;
        console.log('chosen task id',id);
        let list_of_periodic_tasks =  this.props.ProjectTasks[0].tasks;

        list_of_periodic_tasks.forEach((periodicTask) => {
                if (periodicTask.crontab == null)
                {
                    console.log('chosen task id2',id);
                    table_rows.push(<IntervalRow periodicTasks={this.props.ProjectTasks} periodicTask={periodicTask.name} interval={periodicTask.interval} model_id={id} key={`interval_${periodicTask.interval.id}`}/>);
                }
            });
        table_rows.push(<InsertNewIntervalComponent periodicTasks={this.props.ProjectTasks} chosenTask={this.props.ChosenGitRepoTask} key="new_interval_input"/>);
        return (
            <table className="table table-responsive">
                <thead>
                <tr className="success">
                    <th className="text-center">יחידת זמן</th>
                    <th className="text-center">זמן</th>
                    <th className="text-center">
                        <button id="delete_interval" className="btn btn-primary" >
                        <image className="glyphicon glyphicon-wrench"/>
                        </button>
                    </th>
                </tr>
                </thead>
                <tbody className="info">{table_rows}</tbody>
            </table>
        );
    }

    createTableOfCrontab() {

            let table_rows = [];
            let id =this.props.ChosenGitRepoTask.id;
            let list_of_periodic_tasks =  this.props.ProjectTasks[0].tasks;

             list_of_periodic_tasks.forEach((periodicTask) => {
                    if (periodicTask.interval == null)
                    {

                        table_rows.push(<CrontabRow periodicTasks={this.props.ProjectTasks} periodicTask={periodicTask.name} crontab={periodicTask.crontab} model_id={id} key={`crontab_${periodicTask.crontab.id}`}/>);
                    }
                });
        table_rows.push(<InsertNewCrontabComponent periodicTasks={this.props.ProjectTasks} chosenTask={this.props.ChosenGitRepoTask} key="new_interval_input"/>);
        return (
            <table className="table table-responsive">
                <thead>
                <tr className="success">
                    <th className="text-center">דקות</th>
                    <th className="text-center">שעות</th>
                    <th className="text-center">יום בשבוע</th>
                    <th className="text-center">יום בחודש</th>
                    <th className="text-center">חודש</th>
                    <th className="text-center">
                        <image className="glyphicon glyphicon-wrench"/>
                    </th>
                    <th className="text-center"></th>
                    <th className="text-center"></th>



                </tr>
                </thead>
                <tbody className="info">{table_rows}</tbody>
            </table>
        );
    }

    createRunNowButton() {
        return(
            <button className="btn btn-primary" onClick={this.props.runProjectNow}>
                        <p>הרץ כעת</p>
                        <image className="glyphicon glyphicon-play" />
            </button>
        );
    }

    createUpdateRepoButton() {
        return(
            <button className="btn btn-primary" onClick={this.props.updateRepo}>
                        <p>עדכן פרוייקט</p>
                        <image className="glyphicon glyphicon-cloud-download" />
            </button>
        );
    }

    loadTablesAndBUttons =() => {
        return(
            <div>
                {this.createUpdateRepoButton()}
                {this.createRunNowButton()}
                {this.createTableOfInterval(this.props.ProjectTasks)}
                {this.createTableOfCrontab(this.props.ProjectTasks)}
            </div>
        )
    };

    render() {
        return (
            <div>
                <h2>{this.props.ChosenGitRepoTask.repo_name}</h2>
                <h4>{this.props.ChosenGitRepoTask.repo_url}</h4>   
                {(this.props.ProjectTasks.length > 0)? this.loadTablesAndBUttons() : console.log("Nothing to show")}
            </div>
        );
    }


}

export default ManageTaskComponent;