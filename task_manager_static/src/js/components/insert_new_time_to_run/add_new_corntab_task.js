/**
 * Created by lidor08 on 12/09/16.
 */
import React from 'react';
// import InsertNewCrontabComponent from './insert_new_crontab_component'
import {observer} from 'mobx-react';
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'
// import DateTimePicker from 'react-datetimepicker-bootstrap';

@observer
class AddNewContrabTask extends React.Component {

    constructor(props) {
        super(props);
        this.submitNewCrontab = this.submitNewCrontab.bind(this);
        this.handleOptionChange = this.handleOptionChange.bind(this);
        this.DateElement = this.DateElement.bind(this);
        this.everyEndOfMonth = this.everyEndOfMonth.bind(this);
        this.exitButtonSwitch = this.exitButtonSwitch.bind(this);
        this.changeAlertState =this.changeAlertState.bind(this);
        this.initState = this.initState.bind(this);
        this.state ={selectedRadioOption:'',
            daySelect:'*',
            hour:'*',
            minute:'*****',
            time:'',
            date:'',
            end_of_month:false,
            close_div:true,
            alert_div:false};

    };

    exitButtonSwitch(){
        this.setState({close_div: false});
    }

    initState(){
        this.state ={selectedRadioOption:'',
            daySelect:'*',
            hour:'*',
            minute:'*****',
            time:'',
            date:'',
            end_of_month:false,
            close_div:true,
            alert_div:false};

    }
    handleOptionChange(changeEvent)
    {

        console.log(changeEvent.target.value);
        console.log(changeEvent.target.id);
        console.log(this.state.daySelect);

        let newState = this.state;
        newState[changeEvent.target.id] = changeEvent.target.value;
        this.setState(newState);
        if(this.state.selectedRadioOption=='every_end_of_month')
        {
            console.log('handleOptionChange');
             this.everyEndOfMonth();
        }

    }
    everyEndOfMonth(){
        this.setState({end_of_month:true});


    }
    changeAlertState()
    {
        this.setState({alert_div:false})
    }

    submitNewCrontab() {
        let Minute = this.state['time'].slice(3,5);
        let Hour = this.state['time'].slice(0,2);
        let DayOfWeek = this.state['daySelect'];
        let DayOfMonth = this.state['date'].slice(8,10);
        let MonthOfYear = this.state['date'].slice(5,7);
        DayOfMonth = DayOfMonth != '' ? DayOfMonth : '*';
        MonthOfYear = MonthOfYear != '' ? MonthOfYear : '*';
        let repoId = this.props.ChosenGitRepoTask.id;
        let task_type = this.state['selectedRadioOption'];



        if(this.state['day_of_month'])
        {
            Minute='0';
            Hour='0';
            DayOfMonth ='0';
        }
        if(!this.state.end_of_month && this.state.date =='' && this.state.time =='')
        {
                this.setState({alert_div:true});
                console.log("you have to insert a input !!");

        }
        else {
            let formData = {
                minute: Minute,
                hour: Hour,
                day_of_week: DayOfWeek,
                day_of_month: DayOfMonth,
                month_of_year: MonthOfYear,
                Repo_id: repoId,
                interval_or_crontab: 0,
                type: task_type,
            };
            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.addNewScheduleTask(formData, (newData) => {
                this.props.ProjectTasks.filter(project => project.model_id == repoId)[0].tasks.push(newData);
            })


            this.setState({showAddTaskDiv: false});
        }
        this.setState({end_of_month :false});

        this.initState();


    }

    DateElement(){

        switch(this.state['selectedRadioOption']){
            case "every_hour" :
                return (
                    <input id="time" type="time"  onChange={this.handleOptionChange} />
                );
            case "every_day_at" :
                return(
                    <div className="dropdown" id="date_elements">
                        <select value={this.state.daySelect} id="daySelect" className="selectpicker" defaultValue='sunday' onChange={this.handleOptionChange}>
                            <option value={'sunday'}>יום א'</option>
                            <option value={'monday'}>יום ב'</option>
                            <option value={'tuesday'}>יום ג'</option>
                            <option value={'wednesday'}>יום ד'</option>
                            <option value={'thursday'}>יום ה'</option>
                            <option value={'friday'}>יום ו'</option>
                            <option value={'saturday'}>יום ש'</option>
                        </select>
                        <input id="time" type="time"  onChange={this.handleOptionChange}/>
                    </div>


                );
            case "every_date" :
                return(
                    <div id="date_elements">
                        <input id="date"  type="date" onChange={this.handleOptionChange}/>
                        <input id="time" type="time"  onChange={this.handleOptionChange}/>
                    </div>

                );
            case 'every_end_of_month':
                return(<div></div>);

        }
    }




    render() {

        if(this.state.close_div){
            return (
                <div className="new_task_div">
                    <div className="radio-buttons">
                            <label id="radio-button-label">
                                <input type="radio" id="selectedRadioOption"  value="every_hour" onChange={this.handleOptionChange} checked={this.state.selectedRadioOption==='every_hour'}/>
                                כל יום בשעה
                            </label>
                            <label id="radio-buttons-label">
                                <input type="radio" id="selectedRadioOption"  value="every_day_at" onChange={this.handleOptionChange} checked={this.state.selectedRadioOption==='every_day_at'}/>
                                כל יום מסוים, בשעה מסוימת
                            </label>
                            <label id="radio-buttons-label">
                                <input type="radio" id="selectedRadioOption"  value="every_date" onChange={this.handleOptionChange} checked={this.state.selectedRadioOption==='every_date'}/>
                                בתאריך מסוים
                            </label>
                            <label id="radio-buttons-label">
                                <input  type="radio" id="selectedRadioOption"  value="every_end_of_month" onChange={this.handleOptionChange} checked={this.state.selectedRadioOption==='every_end_of_month'}/>
                                כל סוף חודש
                            </label>
                    </div>

                    <div className="date_element">
                            {this.DateElement()}
                    </div>

                    <button className="btn btn-primary btn-round" onClick={this.submitNewCrontab}>
                        <i className="material-icons">schedule</i> הוסף משימה
                    </button>

                    {this.state.alert_div ?
                        <div className="alert alert-danger">
                            <div className="container-fluid">
                              <div className="alert-icon">
                                <i className="material-icons">error_outline</i>
                              </div>
                                  <button type="button" className="close" onClick={this.changeAlertState}>
                                    <i className="material-icons">clear</i>
                                  </button>
                                  <b>שגיאה:</b>עלייך להכניס נתוני זמן בשביל להוסיף משימה!
                            </div>
                        </div>:false}


                </div>

            );
        }
        else {
            return <div></div>;

        }


    }

}

export default AddNewContrabTask;
