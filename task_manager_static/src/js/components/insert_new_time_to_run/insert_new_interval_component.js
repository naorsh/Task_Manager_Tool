/**
 * Created by naor on 01/09/16.
 */
import React from 'react'


const InsertNewIntervalComponent = (props) => {
    return (
        <tr>

            <td><input className="form-control input-sm" id="time_unit_input"/></td>
            <td><input className="form-control input-sm" id="amount_input"/></td>
            <td>
                <button id="delete_interval" className="btn btn-primary" onClick={props.submitNewInterval}>
                    <image className="glyphicon glyphicon-plus"/>
                </button>
            </td>
        </tr>
    );
};

export default InsertNewIntervalComponent;