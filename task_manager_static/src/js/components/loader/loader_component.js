/**
 * Created by naor on 29/11/16.
 */
import React from 'react'

const loaderComponent = () =>
    <div className="loader"></div>;

export default loaderComponent;