/**
 * Created by naor on 23/08/16.
 */
import React, { PropTypes } from 'react';
import { observer } from 'mobx-react';
import Link from 'react-router/lib/Link';
import {Button} from 'react-bootstrap'
import LoaderComponent from './../loader/loader_component';



const RootComponent = observer( props =>

     <div>
         <nav className="navbar navbar-default" role="navigation">
             <div className="container-fluid">
             <div className="navbar-header">
                 {/*<a href="#" className="navbar-brand-right" style={{float: 'right'}}>מתזמן משימות</a>*/}
                 {/*<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" style={{float: 'left'}}>*/}
                     {/*/!*<span class="sr-only">Toggle navigation</span>*!/*/}
                     {/*<span className="icon-bar"></span>*/}
                     {/*<span className="icon-bar"></span>*/}
                     {/*<span className="icon-bar"></span>*/}
                 {/*</button>*/}
                 <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                     <ul className="nav navbar-nav">
                         <li><Link to={'/api/task_manager_app/esg/'}>נהל משימה קיימת</Link></li>
                         <li><Link to={'/api/task_manager_app/esg/insert_new_git_repo/'}>הוסף פרויקט</Link></li>
                     </ul>
                 </div>
             </div>
             </div>
         </nav>
         <div className="container">
             <div className="jumbotron text-center">
                 {
                     props.stores.TasksStore.loading ?
                         <LoaderComponent />
                         :
                      <div id="chosen_child_div">
                         {props.children && React.cloneElement(props.children, { ...props.stores })}
                     </div>
                 }
             </div>
         </div>
     </div>
);
export default RootComponent;