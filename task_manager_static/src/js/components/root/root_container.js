/**
 * Created by naor on 23/08/16.
 */
import React, { Component, PropTypes } from 'react';
import * as stores from '../../stores';
import RootComponent from './root_component';

class Root extends Component {

    render() {
        return (
            // <RootComponent children={this.props.children} stores={stores} />
            <RootComponent children={this.props.children} stores={stores}/>
        );
    }
}

Root.propTypes = {
    children: PropTypes.object.isRequired,
};

export default Root;