/**
 * Created by naor on 23/08/16.
 */
import React from 'react';
import Route from 'react-router/lib/Route';
import IndexRoute from 'react-router/lib/IndexRoute';
import Root from './components/root/root_container';
import InsertNewRepo from './components/insert_new_repo/insert_new_repo_container';
import ChooseRepo from './components/choose_repo/choose_repo_container';
import NewRepoSubmitted from './components/insert_new_repo/new_repo_submitted';


const routes = (
    <Route path="/api/task_manager_app/esg/" component={Root}>
        <IndexRoute component={ChooseRepo} />
        <Route path="/api/task_manager_app/esg/insert_new_git_repo/" component={InsertNewRepo} />
        <Route path="/api/task_manager_app/esg/new_repo_submitted/" component={NewRepoSubmitted} />
    </Route>
);


export default routes;