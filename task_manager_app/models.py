from __future__ import unicode_literals

import json

from django.db import models
from djcelery.models import PeriodicTask, CrontabSchedule, IntervalSchedule
from fabric.api import execute

from task_manager_app.utils.fabfile import *


TASK_NAME = 'task_manager_app.tasks.run_project'


class GitRepo(models.Model):
    main_file_path = models.CharField(max_length=200)
    repo_url = models.CharField(max_length=200)
    repo_name = models.CharField(max_length=200)
    repo_dir_path = models.CharField(max_length=200, default="main.py")
    repo_virtual_env = models.CharField(max_length=200)



    def __str__(self):
        return '%s' % self.repo_name

    def __unicode__(self):
        return '%s' % self.repo_name



class ProjectTasks(models.Model):
    paths = models.ForeignKey(GitRepo, null=True, blank=True)
    name = models.CharField(max_length=200)
    tasks = models.ManyToManyField(PeriodicTask, blank=True, null=True, related_name='tasks')


    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return '%s' % self.name


    def stop(self,task_id):
        """pauses the task"""
        ptask = self.tasks.get(name=task_id)
        print ptask
        # ptask = self.task
        ptask.enabled = False
        ptask.save()

    def start(self,task_id):
        """start the task"""
        ptask = self.tasks.get(name=task_id)
        ptask.enabled = True
        ptask.save()

    def terminate(self,task_id):
        """delete the task"""
        ptask = self.tasks.get(name=task_id)
        print ptask
        self.stop(task_id)
        if ptask.interval != None:
            ptask.interval.delete()
        else:
            ptask.crontab.delete()
        ptask.delete()



    def __str__(self):
        return '%s' % self.name

    def __unicode__(self):
        return '%s' % self.name

