from django.contrib import admin

from .models import GitRepo, ProjectTasks

admin.site.register(GitRepo)
admin.site.register(ProjectTasks)