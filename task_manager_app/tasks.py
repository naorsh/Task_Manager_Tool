from __future__ import absolute_import
import os
from task_manager_app.utils.fabfile import run_main_file
from celery import Celery
from celery import task
from django.conf import settings



app = Celery('tasks', backend='amqp', broker=settings.BROKER_URL)


@task()
def run_project_interval(**kwargs):
    print kwargs
    task_path = kwargs['path']
    task_virtual_env = kwargs['virtual_env']
    run_main_file(task_path, task_virtual_env)
    print 'File was execute'

@task()
def run_project_periodic(**kwargs):
    task_path = kwargs['path']
    task_virtual_env = kwargs['virtual_env']
    run_main_file(task_path, task_virtual_env)
    print 'File was execute'


@task
def run_project_test(**kwargs):
    print kwargs
    print 'start runing file ...'
    try:
        os.system((kwargs['path']))
    except:
        print "error in file running"

