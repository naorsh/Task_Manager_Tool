from rest_framework import serializers
from task_manager_app.models import GitRepo


class GitRepoSerializer(serializers.ModelSerializer):
    class Meta:
        model= GitRepo