from rest_framework import serializers
from djcelery.models import PeriodicTask

class PeriodicTaskSerializer(serializers.ModelSerializer):
    class Meta:
        model=PeriodicTask