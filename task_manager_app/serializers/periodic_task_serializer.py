from rest_framework import serializers
from task_manager_app.models import PeriodicTask
from task_manager_app.serializers.crontab_serializer import CrontabSerializer
from task_manager_app.serializers.interval_serializer import IntervalSerializer


class PeriodicTaskSerializer(serializers.ModelSerializer):

    interval = IntervalSerializer()
    crontab = CrontabSerializer()

    class Meta:
        model=PeriodicTask
        fields = ('interval', 'crontab','name','last_run_at','kwargs')