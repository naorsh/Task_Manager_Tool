from rest_framework import serializers
from djcelery.models import CrontabSchedule

class CrontabSerializer(serializers.ModelSerializer):
    class Meta:
        model= CrontabSchedule