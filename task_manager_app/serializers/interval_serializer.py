from rest_framework import serializers
from djcelery.models import IntervalSchedule

class IntervalSerializer(serializers.ModelSerializer):
    class Meta:
        model= IntervalSchedule