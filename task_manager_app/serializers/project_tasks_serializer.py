from rest_framework import serializers
from task_manager_app.models import ProjectTasks
from task_manager_app.serializers.periodic_task_serializer import PeriodicTaskSerializer


class ProjectTasksSerializer(serializers.ModelSerializer):
    tasks = PeriodicTaskSerializer(many=True)

    class Meta:
        model=ProjectTasks