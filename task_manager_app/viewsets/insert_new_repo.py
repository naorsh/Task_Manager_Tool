from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from task_manager_app.utils.repo_handler import RepoHandler
from task_manager_app.models import GitRepo, ProjectTasks
from task_manager_app.utils.fabfile import delete_local_repo_file


# TODO: initiate new timer manager instance that gets GitRepo id and json

class InsertNewRepo(APIView):
    def delete_and_return_response_with_error_log(self,repo_handler,result):
        delete_local_repo_file(repo_handler.repo_dir_path)
        error_log = result
        response_to_return = Response(status=status.HTTP_404_NOT_FOUND, data=error_log)
        return response_to_return

    def insert_repo_to_data_base_and_return_response(self, repo_handler):
        git_repo = GitRepo(repo_url=repo_handler.repo_url, main_file_path=repo_handler.main_file_path,
                           repo_name=repo_handler.repo_name, repo_virtual_env=repo_handler.repo_virtual_env,
                           repo_dir_path=repo_handler.repo_dir_path)
        git_repo.save()
        project_tasks_obj = ProjectTasks(name=git_repo.repo_name, paths=git_repo)
        project_tasks_obj.save()
        if repo_handler.is_requirements_exist():
            response_to_return = Response(status.HTTP_200_OK)
        else:
            response_to_return = Response(status=status.HTTP_202_ACCEPTED,
                                          data="Git cloning was successful,"
                                               " but the requirements file was not found. ")
        return response_to_return

    def try_to_insert_repo_and_return_response(self, repo_handler):
        result = repo_handler.insert_new_repo()
        if result.failed:
            response_to_return = self.delete_and_return_response_with_error_log(repo_handler, result)
        elif not repo_handler.is_main_file_exist():
            delete_local_repo_file(repo_handler.repo_dir_path)
            response_to_return = Response(status=status.HTTP_404_NOT_FOUND, data="Wrong main file name / path.")
        else:
            response_to_return = self.insert_repo_to_data_base_and_return_response(repo_handler)
        return response_to_return

    def post(self, request):
        repo_handler = RepoHandler(request.data["repo_url"], request.data["main_file_path"])
        is_repo_already_exists = GitRepo.objects.filter(repo_url=repo_handler.repo_url).exists()
        if is_repo_already_exists:
            response_to_return = Response(status=status.HTTP_400_BAD_REQUEST, data="The repo already exists")
        else:
            response_to_return = self.try_to_insert_repo_and_return_response(repo_handler)
        return response_to_return
