from djcelery.models import IntervalSchedule, CrontabSchedule, PeriodicTask
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
import json
import uuid
from task_manager_app.models import ProjectTasks, GitRepo
from task_manager_app.serializers.periodic_task_serializer import PeriodicTaskSerializer
from task_manager_app.serializers.project_tasks_serializer import ProjectTasksSerializer

INTERVAL_TASK_TO_RUN_NAME = 'task_manager_app.tasks.run_project_interval'
PERIODIC_TASK_TO_RUN_NAME = 'task_manager_app.tasks.run_project_periodic'



# TODO : add exception as need


class CreateSchudleTasks(APIView):
    def post(self, request):
        try:
            git_repo_obj = GitRepo.objects.get(pk=request.data['Repo_id'])
            project_tasks_obj = ProjectTasks.objects.get(paths__id=git_repo_obj.id)

        except:
            return Response("GitRepo/ProjectTask object filtering failed.")

        if request.data["interval_or_crontab"] == 1:
            print 'interval'
            try:
                interval = IntervalSchedule(every=request.data['time'], period=request.data['time_unit'])
                interval.save()
                print 'check' + git_repo_obj.main_file_path
                task = PeriodicTask(name=uuid.uuid4(), task=INTERVAL_TASK_TO_RUN_NAME,
                                    interval=interval,
                                    kwargs=json.dumps({'path': git_repo_obj.main_file_path, 'virtual_env': git_repo_obj.repo_virtual_env,
                                                       'task_type': request.data['type']}))
                task.save()

                PeriodicTaskSerializer(task)
                project_tasks_obj.tasks.add(task)
                project_tasks_obj.save()
                serializer = PeriodicTaskSerializer(task)
                return Response(data=serializer.data , status=status.HTTP_200_OK)


            except Exception as e:

                return Response(str(e))

        else:
            print 'crontab'
            try:
                crontab = CrontabSchedule(minute=request.data["minute"], hour=request.data["hour"],
                                          day_of_week=request.data["day_of_week"],
                                          day_of_month=request.data["day_of_month"],
                                          month_of_year=request.data["month_of_year"])
                crontab.save()
                print 'check ' + git_repo_obj.main_file_path
                task = PeriodicTask(name=uuid.uuid4(), task=PERIODIC_TASK_TO_RUN_NAME,
                                    crontab=crontab,
                                    kwargs=json.dumps({'path': git_repo_obj.main_file_path,'virtual_env': git_repo_obj.repo_virtual_env
                                                       ,'task_type': request.data['type']}))
                task.save()
                print "print kwargs" + task.kwargs
                PeriodicTaskSerializer(task)
                project_tasks_obj.tasks.add(task)
                project_tasks_obj.save()
                serializer = PeriodicTaskSerializer(task)
                return Response(data=serializer.data, status=status.HTTP_200_OK)

            except Exception as e:
                return Response(str(e))


