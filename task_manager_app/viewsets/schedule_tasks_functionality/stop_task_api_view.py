from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from task_manager_app.models import ProjectTasks,GitRepo


class StopScheduleTask(APIView):
    def post(self,request):
        try:
            git_repo_obj = GitRepo.objects.get(pk=request.data['model_id'])
            project_tasks_obj = ProjectTasks.objects.get(paths__id=git_repo_obj.id)
            project_tasks_obj.stop(request.data['task_id'])
            return Response(status.HTTP_200_OK)
        except Exception as e:
            return Response(str(e))