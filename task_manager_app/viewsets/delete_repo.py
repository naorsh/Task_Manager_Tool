from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from task_manager_app.utils.fabfile import delete_local_repo_file, delete_virtual_env
from task_manager_app.models import GitRepo, ProjectTasks


class DeleteRepo(APIView):
    def get(self, request, repo_name):
        try:
            git_repo_to_delete = GitRepo.objects.get(repo_name=repo_name)
            project_tasks_to_delete = ProjectTasks.objects.get(paths__repo_name=repo_name)
            periodic_task = project_tasks_to_delete.tasks.all()
            for interval_or_crontab_to_delete in periodic_task:
                interval_or_crontab_to_delete.enabled = False
                if interval_or_crontab_to_delete.interval is not None:
                    interval_or_crontab_to_delete.interval.delete()
                else:
                    interval_or_crontab_to_delete.crontab.delete()
            project_tasks_to_delete.tasks.all().delete()
            delete_local_repo_file(git_repo_to_delete.repo_dir_path)
            delete_virtual_env(git_repo_to_delete.repo_virtual_env)
            git_repo_to_delete.delete()
            return Response(status.HTTP_200_OK)
        except:
            return Response()
