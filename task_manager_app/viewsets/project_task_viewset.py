from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from task_manager_app.models import ProjectTasks
from task_manager_app.serializers.project_tasks_serializer import ProjectTasksSerializer

class ProjectTasksViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = ProjectTasks.objects.all()
    serializer_class = ProjectTasksSerializer