from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from task_manager_app.utils.fabfile import git_pull, pip_install_requirements
from task_manager_app.models import GitRepo


class UpdateRepoByName(APIView):
    def get(self,request, repo_name):
        git_repo_to_update = GitRepo.objects.get(repo_name=repo_name)
        git_pull(repo_url=git_repo_to_update.repo_url, repo_dir_path=git_repo_to_update.repo_dir_path)
        try:
            pip_install_requirements(repo_virtual_env=git_repo_to_update.repo_virtual_env,
                                     repo_dir_path=git_repo_to_update.repo_dir_path)
        except:
            pass
        return Response(status.HTTP_200_OK)
