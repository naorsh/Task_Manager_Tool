from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from task_manager_app.utils.fabfile import run_main_file


class RunTaskNow(APIView):
    def post(self, request):
        print (request.data["main_file_path"], request.data["repo_virtual_env"])
        try:
            print ("the path to the main file is: " + request.data["main_file_path"])
            run_main_file(request.data["main_file_path"], request.data["repo_virtual_env"])
        except:
            return Response(status.HTTP_403_FORBIDDEN)
        return Response(status.HTTP_200_OK)
