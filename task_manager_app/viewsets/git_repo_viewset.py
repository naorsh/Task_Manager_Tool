from rest_framework.viewsets import ModelViewSet
from rest_framework_extensions.mixins import NestedViewSetMixin
from task_manager_app.models import GitRepo
from task_manager_app.serializers.git_repo_serializer import GitRepoSerializer

class GitRepoViewSet(NestedViewSetMixin, ModelViewSet):
    queryset = GitRepo.objects.all()
    serializer_class = GitRepoSerializer