from __future__ import unicode_literals

from django.apps import AppConfig


class TaskManagerAppConfig(AppConfig):
    name = 'task_manager_app'
