# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-29 10:04
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('task_manager_app', '0004_auto_20160829_0840'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='ProjectTaks',
            new_name='ProjectTasks',
        ),
    ]
