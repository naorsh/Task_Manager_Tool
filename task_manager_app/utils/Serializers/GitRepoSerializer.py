from rest_framework import serializers


class GitRepoSerializer(serializers.Serializer):
    main_file_path = serializers.CharField(max_length=200)
    repo_url = serializers.CharField(max_length=200)
    repo_name = serializers.CharField(max_length=200)
    repo_dir_path = serializers.CharField(max_length=200, default="main.py")
    repo_virtual_env = serializers.CharField(max_length=200)