from fabric.context_managers import prefix
from fabric.operations import local as run_local
from fabric.api import settings
from StringIO import StringIO


def git_clone(repo_url, repo_dir_path):
    with settings(warn_only=True):
        run_local_result = run_local("git clone {0} {1}".format(repo_url, repo_dir_path), capture=True)
        return run_local_result


def git_pull(repo_url, repo_dir_path):
    with settings(warn_only=True):
        run_local_result = run_local("cd {repo_dir_path} && git pull {repo_url}".format(repo_dir_path=repo_dir_path, repo_url=repo_url))
        run_local("cd ../..")
        return run_local_result


def make_virtual_env(virtual_env_name):
    # run_local("pip freeze")
    run_local("/bin/bash -l -c 'source ~/.bashrc && mkvirtualenv {virtual_env_name}'".format(virtual_env_name=virtual_env_name))


def pip_install_requirements(repo_virtual_env, repo_dir_path):
    with settings(warn_only=True):
        run_local_result = run_local(
            "/bin/bash -l -c 'source ~/.bashrc && workon {repo_virtual_env} && pip install -r {repo_dir_path}/requirements.txt'".format(
                repo_virtual_env=repo_virtual_env, repo_dir_path=repo_dir_path), capture=True)
        return run_local_result


def run_main_file(repo_main_file_path, repo_virtual_env):
    with settings(warn_only=True):
        run_local_result = run_local("/bin/bash -l -c 'source ~/.bashrc && workon {repo_virtual_env} &&  python {repo_main_file_path}'".format(
            repo_main_file_path=repo_main_file_path, repo_virtual_env=repo_virtual_env), capture=True)
        return run_local_result


def delete_virtual_env(repo_virtual_env):
    with settings(warn_only=True):
        run_local_result = run_local("/bin/bash -l -c  'source ~/.bashrc && rmvirtualenv {repo_virtual_env}'".format(
            repo_virtual_env=repo_virtual_env), capture=True)
    return run_local_result


def delete_local_repo_file(repo_dir_path):
    with settings(warn_only=True):
        run_local_result = run_local("rm -r -f {repo_dir_path}".format(repo_dir_path=repo_dir_path), capture=True)
    return run_local_result
