from fabric.api import execute
import os.path
import fileinput
from task_manager_app.utils.fabfile import *


class RepoHandler:

    def __init__(self, repo_url, main_file_path="main.py"):
        self.repo_url = repo_url
        self.repo_name = self.create_repo_name_from_url()
        self.repo_dir_path = "repositories/" + self.repo_name
        self.main_file_path = "{repo_dir_path}/{main_file_path}"\
            .format(repo_dir_path=self.repo_dir_path, main_file_path=main_file_path) # TODO: add input check for the path
        self.repo_virtual_env = self.repo_name

    def create_repo_name_from_url(self):
        repo_name = self.repo_url.split(".git")[0].split("/")[-1]
        return repo_name

    def create_repo_directory_path_from_url(self):
        repo_dir = "repositories/" + self.repo_name
        return repo_dir

    def clone_repo_from_git(self):
        result = git_clone(repo_url=self.repo_url, repo_dir_path=self.repo_dir_path)
        return result

    def pull_repo_from_git(self): #TODO: is the method receiving repo_url or repo_name?
        git_pull(self.repo_url, self.repo_dir_path)

    def create_virtual_env(self):
        make_virtual_env(self.repo_virtual_env)

    def replace_git_https_links_in_requirements_with_ssh_links(self):
        https_link_str_to_find = "https://gitlab.com/"
        ssh_link_str_to_replace = "ssh://git@gitlab.com/"
        requirements_file = fileinput.FileInput("{repo_dir_path}/requirements.txt".format(repo_dir_path=self.repo_dir_path), inplace=True, backup='.bak')
        for line in requirements_file:
            print line.replace(https_link_str_to_find, ssh_link_str_to_replace)
        requirements_file.close()

    def install_requirements_from_txt(self):
        self.replace_git_https_links_in_requirements_with_ssh_links()
        result = pip_install_requirements(repo_virtual_env=self.repo_virtual_env, repo_dir_path=self.repo_dir_path)
        return result

    def is_requirements_exist(self):
        return os.path.isfile("{repo_dir_path}/requirements.txt".format(repo_dir_path=self.repo_dir_path))

    def is_main_file_exist(self):
        return os.path.isfile(self.main_file_path)

    def insert_new_repo(self):
        result = self.clone_repo_from_git()
        if not result.failed:
            self.create_virtual_env()
            if self.is_requirements_exist():
                result = self.install_requirements_from_txt()
        return result
