from django.conf.urls import url, include
from django.contrib import admin
from rest_framework_extensions.routers import ExtendedDefaultRouter

from task_manager_app.viewsets.delete_repo import DeleteRepo
from task_manager_app.viewsets.get_task_list_from_query import getTaskListFromQuery
from task_manager_app.viewsets.git_repo_viewset import GitRepoViewSet
from task_manager_app.viewsets.insert_new_repo import InsertNewRepo
from task_manager_app.viewsets.project_task_viewset import ProjectTasksViewSet
from task_manager_app.viewsets.run_task_now import RunTaskNow
from task_manager_app.viewsets.schedule_task_apiview import CreateSchudleTasks
from task_manager_app.viewsets.schedule_tasks_functionality.delete_task_api_view import DeleteScheduleTask
from task_manager_app.viewsets.schedule_tasks_functionality.start_task_api_view import StartScheduleTask
from task_manager_app.viewsets.schedule_tasks_functionality.stop_task_api_view import StopScheduleTask
from task_manager_app.viewsets.update_repo_by_name import UpdateRepoByName

admin.autodiscover()

router = ExtendedDefaultRouter()
(
    router.register('git_repos', GitRepoViewSet, base_name='git_repos')
        .register('project_tasks', ProjectTasksViewSet, base_name='project_tasks', parents_query_lookups=['paths'])
)
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url('', include(router.urls)),
    url(r'^get_task_list_from_query/$', getTaskListFromQuery.as_view(), name='get_task_list_from_query'),
    url(r'^insert_new_repo/$', InsertNewRepo.as_view(), name='insert_new_repo'),
    url(r'^create_schudle_tasks/$', CreateSchudleTasks.as_view(), name='create_schudle_tasks'),
    url(r'^delete_schedule_task/$',DeleteScheduleTask.as_view(),name='delete_schedule_task'),
    url(r'^stop_schedule_task/$',StopScheduleTask.as_view(),name='stop_schedule_task'),
    url(r'^start_schedule_task/$',StartScheduleTask.as_view(),name='start_schedule_task'),
    url(r'^update_repo_by_name/(?P<repo_name>.*)/$', UpdateRepoByName.as_view(), name='update_repo_by_name'),
    url(r'^delete_repo/(?P<repo_name>.*)/$', DeleteRepo.as_view(), name='delete_repo'),
    url(r'^run_task_now/$', RunTaskNow.as_view(), name='run_task_now')
]


