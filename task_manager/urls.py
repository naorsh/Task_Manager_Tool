from django.conf.urls import include, url

import task_manager_app.urls
from task_manager.views import IndexView

urlpatterns = [
    url(r'^api/task_manager_app/', include(task_manager_app.urls)),
    url(r'^.*$', IndexView.as_view(), name='index'),
]