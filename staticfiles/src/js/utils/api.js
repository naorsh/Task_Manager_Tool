import axios from 'axios'

export function getTaskList(callback) {
    axios.get('/api/task_manager_app/get_task_list_from_query/')
        .then((tasksList) => {
            callback(tasksList.data);
        });
}


export function getSpecificTaskByGitRepoId(gitRepoId, callback){
    axios.get(`/api/task_manager_app/git_repos/${gitRepoId}/project_tasks/`)
        .then((project_tasks) =>{
            callback(project_tasks.data[0])
        })
}

export function addNewScheduleTask(formData, callback) {
    axios.post('/api/task_manager_app/create_schudle_tasks/',formData)
        .then((periodicTasks) => {
            callback(periodicTasks.data);
        })
}

export function deleteScheduleTask(formData, callback){
    axios.post('/api/task_manager_app/delete_schedule_task/',formData)
        .then((periodicTasks) => {
            callback(periodicTasks.data);
        })

}

export function stopScheduleTask(formData, callback){
    axios.post('/api/task_manager_app/stop_schedule_task/',formData)
        .then((response) => {
            callback(response);
    })

}


export function startScheduleTask(formData, callback){
    axios.post('/api/task_manager_app/start_schedule_task/',formData)
        .then((response) => {
            callback(response);
    })
}




export function runTaskNow(formData, callback) {
    console.log("this is the form data: ", formData);
    axios.post('/api/task_manager_app/run_task_now/', formData)
        .then((response) => {
            callback(response);
    })
}

export function updateRepoByName(repoName, callback){
    axios.get(`/api/task_manager_app/update_repo_by_name/${repoName}/`)
        .then((response) =>{
            callback(response)
        })
}

export function deleteRepoByName(repoName, callback){
    axios.get(`delete_repo/${repoName}/`)
        .then((response) =>{
            callback(response)
        })
}