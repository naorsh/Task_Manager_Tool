/**
 * Created by naor on 25/08/16.
 */
import React, { PropTypes } from 'react';

const InsertNewRepoComponent = props => {
    return(
        <div>
            <label>הכנס קישור לגיט (SSH)</label>
            <input id="link_to_git_input" type="text" className="form-control"/>
            <label>הכנס מיקום קובץ ראשי (Main.py) </label>
            <input id="content_main" type="text" className="form-control" defaultValue="main.py" dir="ltr"/>
            <br/>
            <button className="btn btn-primary" onClick={props.submit_new_repo}>submit</button>
            <div id="output_div"></div>
        </div>
    );
};


export default InsertNewRepoComponent;