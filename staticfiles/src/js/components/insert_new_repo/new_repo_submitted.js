/**
 * Created by naor on 29/11/16.
 */
import React from 'react'
import {Link} from 'react-router'

const NewRepoSubmitted = () =>
    <div>
        <div id="output_div"></div>
        <Link to={`/task_manager_app/`}>
            <button className="btn btn-primary">לדף ניהול המשימות</button>
        </Link>
        <Link to={`/task_manager_app/insert_new_git_repo/`}>
            <button className="btn btn-primary">הוסף פרוייקט נוסף</button>
        </Link>
    </div>;

export default NewRepoSubmitted;