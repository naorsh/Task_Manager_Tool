/**
 * Created by naor on 01/09/16.
 */
import React from 'react';
import InsertNewIntervalComponent from './insert_new_interval_component'
import * as api from '../../utils/api'
import {observer} from 'mobx-react';
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'



@observer
class InsertNewInterval extends React.Component {

    constructor(props) {
        super(props);
        this.submitNewInterval = this.submitNewInterval.bind(this)
    };

     submitNewInterval(){
        let timeUnit =document.getElementById('time_unit_input').value;
        let AmountTime = document.getElementById('amount_input').value;
        let repoId = this.props.ChosenGitRepoTask.id;

        let formData = {
            time: AmountTime,
            time_unit: timeUnit,
            Repo_id: repoId,
           interval_or_crontab: 1
        };

        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.addNewScheduleTask(formData, (newData) => {
            console.log(newData);
            this.props.ProjectTasks[0] = newData;
        })
    };

    render(){
        return(
            <InsertNewIntervalComponent submitNewInterval={this.submitNewInterval}/>
        );
    }

}

export default InsertNewInterval;

