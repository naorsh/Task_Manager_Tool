/**
 * Created by lidor08 on 12/09/16.
 */
import React from 'react'

const InsertNewCrontabComponent = (props) => {
  return(
      <tr>
          <td><input className="form-control input-sm" id='minute_input'/></td>
          <td><input className="form-control input-sm" id='hour_input'/></td>
          <td><input className="form-control input-sm" id='day_of_week_input'/></td>
          <td><input className="form-control input-sm" id='day_of_month_input'/></td>
          <td><input className="form-control input-sm" id='month_of_year_input'/></td>
           <td>
              <button id="delete_interval" className="btn btn-primary" onClick={props.submitNewCrontab}>
                  <image className="glyphicon glyphicon-plus"/>
              </button>
          </td>
      </tr>
  );
};

export default InsertNewCrontabComponent;