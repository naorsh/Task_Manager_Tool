/**
 * Created by naor on 25/08/16.
 */
import React from 'react';
import IntervalRowComponent from './interval_row_component'
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'

class IntervalRow extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return(
            <IntervalRowComponent interval={this.props.interval} deleteIntervalTask = {this.deleteIntervalTask.bind(this)}
                                  stopIntervalTask={this.stopIntervalTask.bind(this)}
                                  startIntervalTask={this.startIntervalTask.bind(this)}/>
        );
    }

    stopIntervalTask(){

            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };
            console.log('from data',formData);
            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.stopScheduleTask(formData, function(response) {
                console.log(response);
            })
    };

    deleteIntervalTask(){

            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };
            console.log('from data',formData);
            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.deleteScheduleTask(formData,(newData) => {
            this.props.ProjectTasks[0] = newData;
            })
    };

    startIntervalTask(){
            let formData = {
            task_id: this.props.periodicTask,
            model_id: this.props.model_id,
            };
            console.log('from data',formData);
            axiosDefaults.xsrfCookieName = 'csrftoken';
            axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

            api.startScheduleTask(formData, function(response) {
                console.log(response);
            })
    };

}

export default IntervalRow