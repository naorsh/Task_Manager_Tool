/**
 * Created by naor on 25/08/16.
 */
import React from 'react'
import {observer} from 'mobx-react';
import ChooseRepoComponent from './choose_repo_component'
import * as api from '../../utils/api'

@observer
class ChooseRepo extends React.Component {

    constructor(props) {
        super(props);
        this.getTaskList = this.getTaskList.bind(this);
        // this.getPeriodicTasksByGitRepoId = this.getPeriodicTasksByGitRepoId.bind(this);
        this.getAllperiodicTasks=this.getAllperiodicTasks.bind(this);
    }

    componentWillMount() {
        this.getTaskList();
    }


    getTaskList() {
        api.getTaskList((allTasks) =>{
            this.props.TasksStore.GitRepoTasksNames = allTasks;
            this.getAllperiodicTasks();

        });

    };

    getAllperiodicTasks(){
        this.props.TasksStore.ProjectTasks = [];
        this.props.TasksStore.GitRepoTasksNames.forEach((gitRepoTask)=>
        {

            api.getSpecificTaskByGitRepoId(gitRepoTask.id, (projectTask) =>{
                this.props.TasksStore.ProjectTasks.push({'task_name':projectTask.name,'model_id':gitRepoTask.id,'tasks':projectTask.tasks})

            })

        });
    }


    // getPeriodicTasksByGitRepoId(id) {
    //     this.props.ChosenGitRepoTask = this.props.TasksStore.GitRepoTasksNames.filter(task => task.id == id)[0];
    //     // this.props.TasksStore.ChosenGitRepoTask = this.props.TasksStore.GitRepoTasksNames.filter(task => task.id == id)[0];
    //     api.getSpecificTaskByGitRepoId(id, (ProjectTasks) =>{
    //         this.props.TasksStore.ProjectTasks = ProjectTasks;
    //     })
    // }


    render() {
        return (
            <ChooseRepoComponent GitRepoTasksNames={this.props.TasksStore.GitRepoTasksNames}
                                 ChosenGitRepoTask={this.props.TasksStore.ChosenGitRepoTask}
                                 getPeriodicTasksByGitRepoId={this.getPeriodicTasksByGitRepoId}
                                 ProjectTasks={this.props.TasksStore.ProjectTasks}
                                 deleteButton={this.props.TasksStore.deleteButton}/>
        );

    };
}

export default ChooseRepo;