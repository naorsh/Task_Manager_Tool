/**
 * Created by lidor08 on 29/09/16.
 */
import React from 'react'
import {observer} from 'mobx-react';
import axiosDefaults from '../../../../../node_modules/axios/lib/defaults'
import * as api from '../../utils/api'

@observer
class PeriodicTasksRow extends React.Component {
    constructor(props) {
        super(props);
        this.deleteCrontabTask = this.deleteCrontabTask.bind(this);
        this.stopCrontabTask = this.stopCrontabTask.bind(this);
        this.startCrontabTask = this.startCrontabTask.bind(this);
        this.state = {PlayStopButton: false};
    }


    stopCrontabTask() {
        let formData = {
            task_id: this.props.name,
            model_id: this.props.model_id,
        };

        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.stopScheduleTask(formData, function (response) {
            console.log(response);
        })
        this.setState({PlayStopButton: true});
        console.log('task stop');
    };


    deleteCrontabTask() {


        let formData = {
            task_id: this.props.name,
            model_id: this.props.model_id,
        };

        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';
        api.deleteScheduleTask(formData, (newData) => {
            let chosenProject = this.props.ProjectTasks.filter(project => project.model_id ==this.props.model_id)[0];
            let indexOfProject =this.props.ProjectTasks.indexOf(chosenProject);
            this.props.ProjectTasks[indexOfProject].tasks =newData.tasks;
        })
        console.log('task delete');

    };


    startCrontabTask() {


        let formData = {
            task_id: this.props.name,
            model_id: this.props.model_id,
        };


        axiosDefaults.xsrfCookieName = 'csrftoken';
        axiosDefaults.xsrfHeaderName = 'X-CSRFToken';

        api.startScheduleTask(formData, function (response) {
            console.log(response);
        })
        this.setState({PlayStopButton: false});
        console.log('task start');
    };



    render() {
        return (
            <tr>
                <td>{this.props.project_name}</td>
                <td>{this.props.period}</td>
                <td>{this.props.last_run}</td>
                <td>
                    {this.state['PlayStopButton']
                        ?
                        <a className="active" onClick={this.startCrontabTask}>
                            <i className="material-icons">play_arrow</i>
                        </a>
                        :
                        <a className="active" onClick={this.stopCrontabTask}>
                            <i className="material-icons">stop</i>
                        </a>
                    }
                    <a className="active" onClick={this.deleteCrontabTask}>
                        <i className="material-icons">delete_forever</i>
                    </a>
                </td>
            </tr>
        );

    }
}


export default PeriodicTasksRow;
