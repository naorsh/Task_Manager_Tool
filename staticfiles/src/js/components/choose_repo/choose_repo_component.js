
/**
 * Created by naor on 25/08/16.
 */
import {observer} from 'mobx-react';
import React from 'react'
import ManageTask from './../manage_task/manage_task_container'
import  PeriodicTasksRow from './PeriodicTasksRow'
import  PeriodicTasksTable from './PeriodicTasksTable'


@observer
class ChooseRepoComponent extends React.Component{
    constructor(props) {
        super(props);
        this.createTasksTable = this.createTasksTable.bind(this);
        this.chosen_project;
        this.days = {'sunday':'א','monday':'ב','tuesday':'ג','wednesday':'ד','thursday':'ה','friday': 'ו','saturday':'ש'}
    };

    mapTasksToOptions(tasksList){
     return tasksList.map( task => {
            return <option key={`task_option_${task.id}`} value={task.id} >{task.repo_name}</option>
        });
    }

    IntervalStringGenerator(type,task){

        var period = {
            'every_seconds':`רץ כל ${task.every} שניות`,
            'every_minutes':`רץ כל ${task.every}דקות`,
            'every_days':`רץ כל ${task.every} ימים`,
            'every_hours':`רץ כל ${task.every} שעות`,
            'microseconds':`רץ כל ${task.every} מילי שיניות`,

        }
        if('every_seconds'==type){
            return period['every_seconds'];

        }  else if('every_minutes'==type){
            return period['every_minutes'];

        }   else if('every_days'===type){
            return period['every_days'];
        }
        else if('every_hours'===type){
            return period['every_hours'];
        }
        else if('microseconds'===type){
            return period['microseconds'];
        }

    }
    CrontabStirngGenerator(type, task)
    {
        var period ={
            'every_hour': ` כל יום בשעה ${task.hour}:${task.minute}`,
            'every_day_at': ` רץ כל יום ${this.days[task.day_of_week]} בשעה ${task.hour}:${task.minute}`,
            'every_date':`רץ כל חודש בתאריך ${task.day_of_month} בשעה${task.hour}:${task.minute}`,
            'every_end_of_month':'רץ כל סוף חודש'

        };
        if('every_hour'==type){
            return period['every_hour'];

        }  else if('every_day_at'==type){
            return period['every_day_at'];

        }   else if('every_date'===type){
            return period['every_date'];
        }
        else if("every_end_of_month"===type){
            return period['every_end_of_month'];
        }


    }




    createTasksTable(){

        let tables =[];
        this.props.ProjectTasks.forEach((periodic_task)=> {
             var lsat_run_str;
             let rows =[];
             this.chosen_project = this.props.GitRepoTasksNames.filter(task => task.id == periodic_task.model_id)[0];
             periodic_task.tasks.forEach((task)=> {
                    let type = JSON.parse(task.kwargs)['task_type'];
                    if (task.crontab) {
                        let period_str = this.CrontabStirngGenerator(type, task.crontab);
                        if (task.last_run_at) {
                            lsat_run_str = (task.last_run_at.slice(0, 10) + ' ' + task.last_run_at.slice(11, 19));
                            rows.push(<PeriodicTasksRow key={task.crontab.id} name={task.name}
                                                        last_run={lsat_run_str} period={period_str}
                                                        ProjectTasks={this.props.ProjectTasks}
                                                        project_name={periodic_task.task_name}
                                                        key_str={this.chosen_project.id}
                                                        model_id={periodic_task.model_id}/>)
                        }
                        else {

                            rows.push(<PeriodicTasksRow key={task.crontab.id} name={task.name}
                                                        last_run={'none'} period={period_str}
                                                        key_str={this.chosen_project.id}
                                                        project_name={periodic_task.task_name}
                                                        ProjectTasks={this.props.ProjectTasks}
                                                        model_id={periodic_task.model_id}/>)

                            // if (task.last_run_at) {
                            //      lsat_run_str = (task.last_run_at.slice(0, 10) + ' ' + task.last_run_at.slice(11, 19));
                            //     rows.push(<PeriodicTasksRow key={task.interval.id}
                            //                                 project_name={periodic_task.task_name}
                            //                                 name={task.name}
                            //                                 last_run={lsat_run_str} period={period_str}
                            //                                 key_str={this.chosen_project.id}
                            //                                 ProjectTasks={this.props.ProjectTasks}
                            //                                 model_id={periodic_task.model_id}/>)
                            // }
                            // else {
                            //     rows.push(<PeriodicTasksRow key={task.interval.id} name={task.name}
                            //                                 last_run={'none'} period={period_str}
                            //                                 project_name={periodic_task.task_name}
                            //                                 ProjectTasks={this.props.ProjectTasks}
                            //                                 key_str={this.chosen_project.id}
                            //                                 model_id={periodic_task.model_id}/>)
                            // }

                        }
                    }
            });
            let periodic_task_for_delete = periodic_task;
            tables.push(<PeriodicTasksTable periodic_task={periodic_task_for_delete} key={periodic_task.id} rows={rows} ChosenGitRepoTask={this.chosen_project} ProjectTasks={this.props.ProjectTasks}/>)

        });

        return tables;


    }

    render(){
        let tables = this.createTasksTable();
        return(
        <div>
            {tables}
        </div>

    );
   }

}

export default ChooseRepoComponent