import { observable } from 'mobx';

class TasksStore {
	@observable GitRepoTasksNames = [];
	@observable ChosenGitRepoTask = {};
	@observable ProjectTasks =[];
	@observable loading = false;
}
export default new TasksStore();
