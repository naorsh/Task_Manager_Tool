#!/bin/bash

NAME="task_manager"
DJANGODIR=/home/esg/task_manager
NUM_WORKERS=3
USER=esg
GROUP=www-data
DJANGO_SETTINGS_MODULE=task_manager.settings
DJANGO_WSGI_MODULE=task_manager.wsgi
echo "Starting $NAME as `whoami`"
echo "`pwd`"
cd $DJANGODIR
source /home/esg/.virtualenvs/task_manager/bin/activate
export DJANGO_SETTINGS_MODULE=task_manager.settings
export PYTHONPATH=$DJANGODIR:$PYTHONPATH
exec /home/esg/.virtualenvs/task_manager/bin/gunicorn task_manager.wsgi:application --name $NAME --workers $NUM_WORKERS --user=$USER -b 0.0.0.0:8003 --log-level=debug --log-file=- --timeout=600
