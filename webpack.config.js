/**
 * Created by naor on 18/08/16.
 */

var path = require('path');
var webpack = require('webpack');

var ROOT_PATH = path.resolve(__dirname, 'task_manager_static');
module.exports = {
    entry: path.resolve(ROOT_PATH, 'src/index'),      // I changed the original entry: src/index
    output: {
        path: path.join(ROOT_PATH, 'build'),
        filename: 'bundle.js'
    },
    eslint: {
        configFile: path.resolve(__dirname, '.eslintrc.js')
    },
    module: {
        loaders: [
            {
                test: /\.css$/,
                exclude: /node_modules/,
                loaders: ['style', 'css'],
                include: path.resolve(ROOT_PATH, 'src/css')
            },
            {
                test: /\.(js|jsx|es6)$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    cacheDirectory: true,
                    presets: ['es2015', 'stage-0', 'react'],
                    plugins: ["transform-decorators-legacy"]
                }
            }
        ]
    }
};